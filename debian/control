Source: vokoscreen
Section: video
Priority: optional
Maintainer: Joao Eriberto Mota Filho <eriberto@debian.org>
Build-Depends: debhelper-compat (= 12),
               libasound2-dev,
               libqt5x11extras5-dev,
               libv4l-dev,
               libx11-dev,
               libxrandr-dev,
               pkg-config,
               qt5-qmake,
               qttools5-dev-tools
Standards-Version: 4.5.0
Rules-Requires-Root: no
Homepage: https://github.com/vkohaupt/vokoscreen
Vcs-Browser: https://salsa.debian.org/debian/vokoscreen
Vcs-Git: https://salsa.debian.org/debian/vokoscreen.git

Package: vokoscreen
Architecture: any
Recommends: libdc1394-utils
Suggests: libaacs0,
          libmp3lame0,
          libvorbis0a,
          libx264-148,
          vlc | dragonplayer | kplayer
Depends: ffmpeg,
         lsof,
         pulseaudio-utils,
         xdg-utils,
         ${misc:Depends},
         ${shlibs:Depends}
Description: easy to use screencast creator
 vokoscreen can be used to record educational videos, live recordings
 of browser, installation, videoconferences, etc. You can capture an
 alone video or video and sound (via ALSA or PulseAudio).
 .
 The program is very simple and uses a minimalistic GUI. It also can
 capture your face using a webcam in the same time, so this feature is
 especially suitable for screencasting purposes. Another feature is the
 direct capture from IEEE1394 digital cameras.
 .
 This program uses the ffmpeg features and saves the capture in some
 formats, as AVI, MP4, FLV and MKV for video and MP3 for audio.
